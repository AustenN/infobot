# INFOBOT

INFOBOT is a matrix bot that users can use to store and retrieve facts.

Commands:
* `infobot: <a> is <b>`: This remembers the fact that `a` is `b`
* `infobot: <a>`: This recalls all facts about `a`
* `infobot: forget <a>`: This forgets all facts about `a`

More features:
* INFOBOT will look for all instances of `<a>++` and `<a>--`, just like a
  karmabot. The number of increments and decrements will be shown in 
  `infobot <a>`
* INFOBOT tries to extract facts from normal messages. If a user types in
  `foo is bar` then INFOBOT will store that fact.

INFOBOT is configured via a `config.toml`. An example configuration file is
provided at `config.toml.sample`, and this should be edited and moved to
`config.toml` to configure the bot. After it's all configured you can run
the bot with a simple `cargo run`
