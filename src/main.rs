//
//    infobot, a matrix bot for storing information on things
//    Copyright (C) 2019 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#[macro_use]
extern crate lalrpop_util;

use matrix_bot_api::handlers::{HandleResult, Message, MessageHandler};
use matrix_bot_api::MessageType;
use rand::prelude::*;
use unqlite::{Cursor, Transaction, UnQLite, KV};

mod command;
mod value;

use command::Command;
use value::Facts;

lalrpop_mod!(pub info); // synthesized by LALRPOP

use matrix_bot_api::{ActiveBot, MatrixBot};

fn main() {
    println!("Starting infobot");
    let mut settings = config::Config::default();
    settings
        .merge(config::File::with_name("config.toml"))
        .unwrap();
    println!("Read in config file");

    // Read configuration from config.toml
    let user = settings.get_str("user").unwrap();
    let password = settings.get_str("password").unwrap();
    let homeserver_url = settings.get_str("homeserver_url").unwrap();
    let db = settings.get_str("db").unwrap();
    let reply = settings.get_bool("reply").unwrap();

    // Load the database
    let handler = InfoStore::new(UnQLite::create(db), reply);
    println!("Loaded DB");
    // And set up the bot
    let bot = MatrixBot::new(handler);
    println!("Starting bot");
    bot.run(&user, &password, &homeserver_url);
}

// This struct lets me create a message handler with state
struct InfoStore {
    data: UnQLite,
    should_reply: bool,
}
impl InfoStore {
    pub fn new(data: UnQLite, should_reply: bool) -> InfoStore {
        InfoStore { data, should_reply }
    }
}
impl MessageHandler for InfoStore {
    fn handle_message(&mut self, bot: &ActiveBot, message: &Message) -> HandleResult {
        // Parse an infobot command and update the DB + send messages accordingly
        if let Ok(parsed_command) = info::MessageParser::new().parse(&message.body) {
            for command in parsed_command {
                match command {
                    Command::Set(key, value) => {
                        //Convert key to lowercase
                        let key = key.to_lowercase();
                        if let Ok(old_value) = self.data.kv_fetch(&key) {
                            // If the bytes parse as
                            if let Some(mut facts) = Facts::from_bytes(old_value.as_slice()) {
                                facts.insert(value.clone());
                                let _ = self.data.kv_store(&key, facts.to_bytes());
                            }
                        } else {
                            // Otherwise just store the value
                            let mut facts = Facts::new();
                            facts.insert(value.clone());
                            let _ = self.data.kv_store(&key, facts.to_bytes());
                        }
                        // Commit the data
                        let _ = self.data.commit();
                        // Avoid spam
                        if value.as_str() != "++" && self.should_reply {
                            bot.send_message(
                                &format!("I will remember that"),
                                &message.room,
                                MessageType::RoomNotice,
                            );
                        }
                    }
                    Command::Forget(key) => {
                        //Convert key to lowercase
                        let key = key.to_lowercase();
                        // Drop the data
                        let _ = self.data.kv_delete(&key);
                        // Commit the change
                        let _ = self.data.commit();
                        bot.send_message(
                            &format!("I forgot {}", key),
                            &message.room,
                            MessageType::RoomNotice,
                        );
                    }
                    Command::Search(ref key) => {
                        if let Ok(value) = self.data.kv_fetch(&key.to_lowercase()) {
                            // If the bytes parse as utf8 send the message out
                            if let Some(mut facts) = Facts::from_bytes(value.as_slice()) {
                                let text = format!("{} is {}", key, facts);
                                facts.render_facts();

                                bot.send_html_message(
                                    &text,
                                    &format!("{} is {}", key, facts),
                                    &message.room,
                                    MessageType::RoomNotice,
                                );
                            }
                        } else {
                            // Let the user know if we can't find a matching entry
                            bot.send_message(
                                &format!("{} not found", key),
                                &message.room,
                                MessageType::RoomNotice,
                            );
                        }
                    }
                    Command::Random => {
                        let mut records = vec![];
                        let mut query = self.data.first();

                        loop {
                            match query {
                                None => break,
                                Some(entry) => {
                                    let record = entry.key_value();
                                    query = entry.next();
                                    records.push(record);
                                }
                            }
                        }

                        let mut rng = rand::thread_rng();
                        let surprise = rng.gen_range(0, records.len());
                        let (key, facts) = records[surprise].clone();
                        if let Ok(key) = String::from_utf8(key) {
                            // If the bytes parse as utf8 send the message out
                            if let Some(mut facts) = Facts::from_bytes(facts.as_slice()) {
                                let text = format!("{} is {}", key, facts);
                                facts.render_facts();

                                bot.send_html_message(
                                    &text,
                                    &format!("{} is {}", key, facts),
                                    &message.room,
                                    MessageType::RoomNotice,
                                );
                            }
                        }
                    }
                }
            }
        }
        // There's no other handler so we don't have to forward any messages
        return HandleResult::StopHandling;
    }
}

#[cfg(test)]
mod test {
    use crate::command::Command;
    use crate::info;
    #[test]
    fn message_parse() {
        let result = info::MessageParser::new().parse("infobot: x is y");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y".to_owned())])
        )
    }
    #[test]
    fn are_message_parse() {
        let result = info::MessageParser::new().parse("infobot: x are y");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y".to_owned())])
        )
    }
    #[test]
    fn long_are_message_parse() {
        let result = info::MessageParser::new().parse("these messages are now valid");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "these messages".to_owned(),
                "now valid".to_owned()
            )])
        )
    }
    #[test]
    fn longer_are_message_parse() {
        let result =
            info::MessageParser::new().parse("these messages are now valid infobot commands");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "these messages".to_owned(),
                "now valid infobot commands".to_owned()
            )])
        )
    }
    #[test]
    fn alt_name_message_parse() {
        let result = info::MessageParser::new().parse("inforbot: x is y");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y".to_owned())])
        )
    }
    #[test]
    fn long_key_parse() {
        let result = info::MessageParser::new().parse("foo bar is baz");
        assert_eq!(
            result,
            Ok(vec![Command::Set("foo bar".to_owned(), "baz".to_owned())])
        )
    }
    #[test]
    fn alt_name_get_message_parse() {
        let result = info::MessageParser::new().parse("inforbot: x");
        assert_eq!(result, Ok(vec![Command::Search("x".to_owned())]))
    }
    #[test]
    fn inline_message_parse() {
        let result = info::MessageParser::new().parse("x is y");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y".to_owned())])
        )
    }
    #[test]
    fn insentence_message_parse() {
        let result = info::MessageParser::new().parse("I believe that that x is y");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y".to_owned())])
        )
    }
    #[test]
    fn inline_increment_parse() {
        let result = info::MessageParser::new()
            .parse("oh yeah pigs++ pigs++ pigs++ for helping me look at my car today");
        assert_eq!(
            result,
            Ok(vec![
                Command::Set("pigs".to_owned(), "++".to_owned()),
                Command::Set("pigs".to_owned(), "++".to_owned()),
                Command::Set("pigs".to_owned(), "++".to_owned()),
            ])
        )
    }
    #[test]
    fn question_message_parse() {
        let result = info::MessageParser::new().parse("could it true that x is y?");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "y?".to_owned())])
        )
    }
    #[test]
    fn long_question_message_parse() {
        let result = info::MessageParser::new().parse("could it true that x is foo bar?");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "foo bar?".to_owned())])
        )
    }
    #[test]
    fn sub_message() {
        let result = info::MessageParser::new().parse("x--");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "--".to_owned())])
        )
    }
    #[test]
    fn add_message() {
        let result = info::MessageParser::new().parse("x++");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "++".to_owned())])
        )
    }
    #[test]
    fn multiadd_message() {
        let result = info::MessageParser::new().parse("x++ x++ y++");
        assert_eq!(
            result,
            Ok(vec![
                Command::Set("x".to_owned(), "++".to_owned()),
                Command::Set("x".to_owned(), "++".to_owned()),
                Command::Set("y".to_owned(), "++".to_owned())
            ])
        )
    }
    #[test]
    fn two_word_add() {
        let result = info::MessageParser::new().parse("one++ and two++");
        assert_eq!(
            result,
            Ok(vec![
                Command::Set("one".to_owned(), "++".to_owned()),
                Command::Set("two".to_owned(), "++".to_owned())
            ])
        )
    }
    #[test]
    fn is_add_message() {
        let result = info::MessageParser::new().parse("x is foo++");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "foo++".to_owned())])
        )
    }
    #[test]
    fn dot_message() {
        let result = info::MessageParser::new().parse("x is foo.");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "foo.".to_owned())])
        )
    }
    #[test]
    fn double_dot_message() {
        let result = info::MessageParser::new().parse("plans is movie night.");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "plans".to_owned(),
                "movie night.".to_owned()
            )])
        )
    }
    #[test]
    fn multi_dot_message() {
        let result = info::MessageParser::new().parse("x is foo. y is bar.");
        assert_eq!(
            result,
            Ok(vec![
                Command::Set("x".to_owned(), "foo.".to_owned()),
                Command::Set("y".to_owned(), "bar.".to_owned())
            ])
        )
    }
    #[test]
    fn long_message_parse() {
        let result = info::MessageParser::new().parse("infobot: x is foo bar");
        assert_eq!(
            result,
            Ok(vec![Command::Set("x".to_owned(), "foo bar".to_owned())])
        )
    }
    #[test]
    fn multiword_key_parse() {
        let result = info::MessageParser::new().parse("infobot: x of y is foo of bar");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "x of y".to_owned(),
                "foo of bar".to_owned()
            )])
        )
    }
    #[test]
    fn colon_message_parse() {
        let result = info::MessageParser::new().parse("infobot: info: is test: one");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "info:".to_owned(),
                "test: one".to_owned()
            )])
        )
    }
    #[test]
    fn is_message_parse() {
        let result = info::MessageParser::new().parse("infobot: x is everything is fine");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "x".to_owned(),
                "everything is fine".to_owned()
            )])
        )
    }
    #[test]
    fn forget_message_parse() {
        let result = info::MessageParser::new().parse("infobot: x is forget me not");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "x".to_owned(),
                "forget me not".to_owned()
            )])
        )
    }
    #[test]
    fn infobot_is_message_parse() {
        let result = info::MessageParser::new().parse("infobot: infobot: is is is");
        assert_eq!(
            result,
            Ok(vec![Command::Set(
                "infobot:".to_owned(),
                "is is".to_owned()
            )])
        )
    }
    #[test]
    fn forget_parse() {
        let result = info::MessageParser::new().parse("infobot: forget x");
        assert_eq!(result, Ok(vec![Command::Forget("x".to_owned())]))
    }
    #[test]
    fn random() {
        let result = info::MessageParser::new().parse("infobot: random fact");
        assert_eq!(result, Ok(vec![Command::Random]))
    }
}
